# TDM1 - L'API Socket

AP_UNSPEC : osef ivp4 / ipv6

1)
  Appli client :
  - Connexion : Ligne 42
  - Transfert de données : Ligne 55
  - Déconnexion : Ligne 59

  Appli serveur :
  - Connexion : Ligne 69
  - Transfert de données : Ligne 83
  - Déconnexion : Ligne 85

2)
  <localhost, ça dépend,localhost, 12345, FTP>

3)
 Elle permet d'essayer de se connecter tant que la connexion échoue. Si la création de la socket échoue, le programme s'arrête avec le code d'erreur 2. S'il n'a plus d'adresse à tester pour établir la connexion, il n'essaye plus de se connecter.

4)
  Non cela ne pose aucun soucis étant donné que les fonctions utilisées pour récupérer l'adresse IP sont uniquement getaddrinfo(). Cette dernière supporte les adresses IPV4 et IPV6, contrairement à la fonction gethostbyname() qui ne supporte que les adresses IPV4.

5)
  Ça correspond au protocole HTTP, car aucun protocole n'est spécifié dans le code.
  Si on change le port par 21 et l'adresse par r-info-onyx ça sera le procole FTP.

6)
  Rajouter le code suivant après la ligne 58 du code client.c :

  strcpy(buffer, "OK\n");
  ecode = write(descSock, buffer, strlen(buffer));
  if(ecode == −1) {perror("Problème  d'écriture\n"); exit(7);}
